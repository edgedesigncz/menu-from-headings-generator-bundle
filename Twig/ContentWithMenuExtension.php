<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Twig;
use Edge\MenuFromHeadingsGeneratorBundle\Generator\MenuFromHeadingsGenerator;
use Twig_SimpleFilter;


/**
 * Extension that adds filter 'contentWithMenu' into Twig.
 * 
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class ContentWithMenuExtension extends \Twig_Extension
{
    /** @var \Twig_Environment */
    private $environment;

    /** @var MenuFromHeadingsGenerator */
    private $menuFromHeadingsGenerator;

    public function __construct(MenuFromHeadingsGenerator $menuFromHeadingsGenerator)
    {
        $this->menuFromHeadingsGenerator = $menuFromHeadingsGenerator;
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->environment = $environment;
    }

    public function getFilters()
    {
        return array(
            new Twig_SimpleFilter('contentWithMenu', array($this, 'contentWithMenu'), array(
                'is_safe' => array('html'),
            )),
        );
    }


    /**
     * This method gets html content with some headings and returns rendered template, that has access to two parameters:
     * 'content' which contains modified html content with added anchors to headings and
     * 'headingNode' which is instance of HeadingNode and is the top-node of newly created menu.
     *
     * @param string $content The content, the menu should be extracted from.
     * @param string $template Logical name of template, that should be used for rendering menu and content.
     * @param array  $templateVariables Array of variables, which should be passed into template. Key is name of variable.
     * @return string
     */
    public function contentWithMenu($content, $template, array $templateVariables = array())
    {
        $response = $this->menuFromHeadingsGenerator->generateMenu($content);

        $variables = array_merge(
            array(
               'content' => $response->getContent(),
                'headingNode' => $response->getHeadingNode(),
            ),
            $templateVariables
        );

        return $this->environment->render($template, $variables);
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'contentwithmenu';
    }
}
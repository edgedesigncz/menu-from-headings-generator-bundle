<?php

namespace Edge\MenuFromHeadingsGeneratorBundle;

/**
 * Daidalos core bundle
 *
 * @author Marek Makovec<marek.makovec@edgedesign.cz>
 */
class EdgeMenuFromHeadingsGeneratorBundle extends \Symfony\Component\HttpKernel\Bundle\Bundle
{

}

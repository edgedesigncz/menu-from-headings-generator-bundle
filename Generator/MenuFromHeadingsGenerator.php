<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Generator;
use DOMElement;
use Edge\MenuFromHeadingsGeneratorBundle\Response\MenuFromHeadingsGeneratorResponse;
use Edge\MenuFromHeadingsGeneratorBundle\Node\HeadingNode;

use FluentDOM;


/**
 * Class for parsing menu structure from given $htmlContent using FluentDOM.
 * It resturns Response containing content with added ID's to headings and HeadingNode structure
 * for creating menu.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class MenuFromHeadingsGenerator
{

    private $anchorId = 0;

    /** @var ItemIdGeneratorInterface */
    private $idGenerator;

    public function __construct(ItemIdGeneratorInterface $idGenerator)
    {
        $this->idGenerator = $idGenerator;
    }

    /**
     * This method can parse given $htmlContent and returns MenuFromHeadingsGeneratorResponse containing content
     * with added anchors for newly created menu and nodes, that can be used to generating this menu.
     *
     * @param $htmlContent
     * @return MenuFromHeadingsGeneratorResponse
     */
    public function generateMenu($htmlContent)
    {
        $fd = new FluentDOM();
        // hack because of stupid DOMDocument, which is internally used by FluentDOM and without proper specification brokes UTF down.
        $htmlContent = '<?xml encoding="UTF-8">' . $htmlContent;

        $fd->load($htmlContent, 'text/html');

        $rootNode = new HeadingNode();
        $parentNode = $rootNode;
        // We have to find, in which level menu structure starts. It typically does not start at level 1, but later.
        // If heading with higher number is before heading with lower number, the previous one will be ignored and forgotten forever.
        $level = 0;
        $children = $fd->find("//*[name()='h1' or name()='h2' or name()='h3' or name() = 'h4' or name() = 'h5' or name() = 'h6']"); // todo: is there some better way how to export all headers?

        foreach ($children as $child) {
            /** @var DOMElement $child */

            $headingLevel = $this->getLevel($child->tagName);
            if ($level === 0) {
                $level = $headingLevel;
            }
            if ($headingLevel > $level) {
                $parentNode = $parentNode->getChildren()->last();
            } else if ($headingLevel < $level) {
                $parentNode = $this->getNthAncestor($parentNode, $level-$headingLevel);
            }
            $level = $headingLevel;
            $node = $this->createHeadingNode($child, $headingLevel);

            $parentNode->addChild($node);
        }
        $html = $this->getBodyHtmlString($fd);

        return new MenuFromHeadingsGeneratorResponse($html, $rootNode);
    }

    /**
     * Creates new HeadingNode from DOMElement
     *
     * @param DOMElement $domNode
     * @param $level
     * @return HeadingNode
     */
    private function createHeadingNode(DOMELement $domNode, $level)
    {
        $node = new HeadingNode();
        $node->setLevel($level);
        $node->setText($domNode->textContent);

        $anchor = $domNode->getAttribute('id');

        if ($anchor === '') {
            $anchor = (string) $this->idGenerator->generate($domNode->textContent);
        }
        $node->setAnchor($anchor);
        $domNode->setAttribute('id', $anchor);

        return $node;
    }

    /**
     * Returns Nth ancestor of given $node.
     * If there is not enough ancestors, cycle stops and top parent is returned.
     *
     * @param HeadingNode $node
     * @param $n
     * @return HeadingNode
     */
    private function getNthAncestor(HeadingNode $node, $n)
    {
        $ancestor = $node;
        for ($i = 0; $i<$n; $i++) {
            $node = $ancestor->getParent();
            if ($node === null) {
                break;
            }
            $ancestor = $node;
        }

        return $ancestor;
    }

    /**
     * Returns level of incursion by heading tag. Basically returns the number in this tag.
     *
     * @param $headingTag
     * @return int
     */
    private function getLevel($headingTag)
    {
        return $headingTag[1];
    }

    /**
     * Extracts <body> from given FluentDOM and converts domElement into string. Then returns it.
     *
     * @param FluentDOM $fd
     * @return string
     */
    private function getBodyHtmlString(FluentDOM $fd)
    {
        $domElement = $fd->find("//*[name()='body']")[0];
        $html = '';

        $children = $domElement->childNodes;
        foreach ($children as $child) {
            $html .= $child->ownerDocument->saveXML($child);
        }

        return $html;
    }
}
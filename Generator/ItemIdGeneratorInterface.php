<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Generator;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
interface ItemIdGeneratorInterface
{
    /**
     * Returns context-unique HTML identifier for menunode.
     * Can contain optional string.
     *
     * @param $string
     *
     * @return string
     */
    public function generate($string = "");

    /**
     * Resets counter for creating identifiers
     *
     * @return void
     */
    public function resetCounter();
} 
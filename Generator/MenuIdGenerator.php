<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Generator;

use Edge\Utils\Type\String;

/**
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class MenuIdGenerator implements ItemIdGeneratorInterface
{
    /**
     * Every identifier must be unique. This generator uses variable
     * counter, to ensure, that every generated heading will unique number
     * in it.
     * @var int
     */
    private $counter;

    /**
     * Maximum length of generated identifier.
     *
     * @var int
     */
    private $maxlength;

    /**
     * prefix of generated identifier.
     * @var string
     */
    private $prefix;

    /**
     * separator between parts of created ID
     * @var string
     */
    private $separator;

    public function __construct($maxlength = 255, $prefix = 'heading', $separator = '-')
    {
        $this->maxlength = $maxlength;
        $this->separator = $separator;
        $this->prefix = $prefix;
        $this->resetCounter();
    }

    /**
     * Returns context-unique HTML identifier for menunode.
     * Can contain optional string.
     *
     * @param $string
     *
     * @return string
     */
    public function generate($string = "")
    {
        $string = new String($string);

        $parts = array();
        if (trim($this->prefix) !== '') {
            $parts[] = $this->prefix;
        }

        $parts[] = $this->counter++;
        $parts[] = $string->webalize()->__toString();

        $identifier = implode($this->separator, $parts);

        return substr($identifier, 0, $this->maxlength);
    }

    /**
     * Resets counter for creating identifiers
     *
     * @return void
     */
    public function resetCounter()
    {
        $this->counter = 1;
    }
}
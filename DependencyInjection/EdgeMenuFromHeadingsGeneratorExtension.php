<?php

namespace Edge\MenuFromHeadingsGeneratorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;


/**
 * extension loading class
 *
 * @author Marek Makovec <marek.makovec@edgedesign.cz>
 */
class EdgeMenuFromHeadingsGeneratorExtension extends Extension
{

    /**
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     *
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

}
<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Node;


/**
 * Composite class containing heading structure.
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class HeadingNode
{

    /** @var string */
    private $anchor;

    /** @var HeadingNode[]  */
    private $children;

    /** @var int */
    private $level;

    /** @var HeadingNode */
    private $parent;

    /** @var string */
    private $text;

    public function __construct()
    {
        $this->children = array();
    }

    /**
     * @param HeadingNode $node
     * @return $this
     */
    public function addChild(HeadingNode $node)
    {
        $this->children[] = $node;
        $node->setParent($this);
        return $this;
    }

    /**
     * @param string $anchor
     */
    public function setAnchor($anchor)
    {
        $this->anchor = $anchor;
    }

    /**
     * @return string
     */
    public function getAnchor()
    {
        return $this->anchor;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return \Edge\Daidalos\CoreBundle\Node\HeadingNode
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return HeadingNode
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Returns lastly added child. If there is none, false is returned.
     *
     * @return bool|HeadingNode
     */
    public function getLastChild()
    {
        if (count($this->children)) {
            return $this->children[count($this->children)-1];
        }

        return false;
    }

    /**
     * @param HeadingNode $node
     * @SuppressWarnings("unused")
     * Warning suppresed, because this method is infact used in method addChildren().
     */
    private function setParent(HeadingNode $node)
    {
        $this->parent = $node;
    }

    /**
     * @param int $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    /**
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }
}
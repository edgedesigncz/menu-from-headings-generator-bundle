<?php


namespace Edge\MenuFromHeadingsGeneratorBundle\Response;
use Edge\MenuFromHeadingsGeneratorBundle\Node\HeadingNode;


/**
 * Response given from MenuFromHeadingsGenerator containing Content and HeadingNode.
 * What an explaining comment!
 *
 * @author: Marek Makovec <marek.makovec@edgedesign.cz>
 */
class MenuFromHeadingsGeneratorResponse
{
    /** @var string */
    private $content;

    /** @var HeadingNode */
    private $headingNode;

    /**
     * @param $content
     * @param HeadingNode $headingNode
     */
    public function __construct($content, HeadingNode $headingNode)
    {
        $this->content = $content;
        $this->headingNode = $headingNode;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return \Edge\Daidalos\CoreBundle\Node\HeadingNode
     */
    public function getHeadingNode()
    {
        return $this->headingNode;
    }


}